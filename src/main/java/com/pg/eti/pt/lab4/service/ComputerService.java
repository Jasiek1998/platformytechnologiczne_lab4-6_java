package com.pg.eti.pt.lab4.service;


import com.pg.eti.pt.lab4.model.Computer;
import com.pg.eti.pt.lab4.repository.ComputerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ComputerService {
    @Autowired
    private ComputerRepository computerRepository;

    public Optional<Computer> addComputer(Computer computer) {
        if (computer.getPrice() == null || computer.getPrice() <= 0 || computer.getAmountAvailable() == null || computer.getAmountAvailable() <= 0) {
            return Optional.empty();
        } else {
            Computer newComputer = new Computer(null, computer.getType(), computer.getProcessorType(), computer.getRam(), computer.getGraphicCard(), computer.getHdd(), computer.getAmountAvailable(), computer.getPrice());
            computerRepository.save(newComputer);
            return Optional.of(newComputer);
        }
    }

    public List<Computer> getAll() {
        return (List<Computer>) computerRepository.findAll();
    }


    public Optional<Computer> update(Computer computer, Long id) {
        if (computerRepository.findById(id).isPresent()) {
            Computer newComputer = fillFields(id, computer);
            computerRepository.save(newComputer);
            return Optional.of(newComputer);
        } else {
            return Optional.empty();
        }
    }

    public Computer getComputerById(Long id) {
        if (computerRepository.findById(id).isPresent()) {
            return computerRepository.findById(id).get();
        } else {
            return null;
        }
    }

    public Computer fillFields(Long id, Computer computer) {
        Computer newComputer = computerRepository.findById(id).get();
        if (computer.getAmountAvailable() != null) {
            newComputer.setAmountAvailable(computer.getAmountAvailable());
        }
        if (computer.getGraphicCard() != null) {
            newComputer.setGraphicCard(computer.getGraphicCard());
        }
        if (computer.getHdd() != null) {
            newComputer.setHdd(computer.getHdd());
        }
        if (computer.getPrice() != null) {
            newComputer.setPrice(computer.getPrice());
        }
        if (computer.getProcessorType() != null) {
            newComputer.setProcessorType(computer.getProcessorType());
        }
        if (computer.getRam() != null) {
            newComputer.setRam(computer.getRam());
        }
        if (computer.getType() != null) {
            newComputer.setType(computer.getType());
        }
        return newComputer;
    }
}
