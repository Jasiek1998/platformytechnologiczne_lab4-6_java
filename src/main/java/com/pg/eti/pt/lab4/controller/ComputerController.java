package com.pg.eti.pt.lab4.controller;


import com.pg.eti.pt.lab4.model.Computer;
import com.pg.eti.pt.lab4.service.ComputerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/computer/")
public class ComputerController {
    private ComputerService computerService;

    @Autowired
    public ComputerController(ComputerService computerService) {
        this.computerService = computerService;
    }

    @PostMapping("/add")
    public Optional<Computer> addComputer(@RequestBody Computer computer) {
        return computerService.addComputer(computer);
    }

    @GetMapping("/getAll")
    public List<Computer> getAll() {
        return computerService.getAll();
    }

    @PutMapping("/update/{id}")
    public Optional<Computer> update(@RequestBody Computer computer, @PathVariable Long id) {
        return computerService.update(computer, id);
    }
}
