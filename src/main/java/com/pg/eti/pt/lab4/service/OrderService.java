package com.pg.eti.pt.lab4.service;

import com.pg.eti.pt.lab4.model.Computer;
import com.pg.eti.pt.lab4.model.Order;
import com.pg.eti.pt.lab4.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    @Autowired
    private ComputerService computerService;
    @Autowired
    private OrderRepository orderRepository;


    public Optional<Order> addOrder(Order order) {
        if (order.getComputerList().size() < 2) {
            return Optional.empty();
        } else {
            for (Computer computer : order.getComputerList()) {
                Computer repoComputer = computerService.getComputerById(computer.getId());
                if (repoComputer.getAmountAvailable() <= 0) {
                    return Optional.empty();
                }
            }
        }
        List<Computer> newList = new ArrayList<>();
        for(Computer computer : order.getComputerList()){
            Computer repoComputer = computerService.getComputerById(computer.getId());
            repoComputer.setAmountAvailable(repoComputer.getAmountAvailable()-1);
            newList.add(repoComputer);
        }
        Order newOrder = new Order(null, newList);
        orderRepository.save(newOrder);
        return Optional.of(newOrder);
    }


    public List<Order> getAllOrders() {
        return (List<Order>) orderRepository.findAll();
    }
}
