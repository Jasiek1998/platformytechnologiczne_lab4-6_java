package com.pg.eti.pt.lab4.repository;

import com.pg.eti.pt.lab4.model.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {
}
