package com.pg.eti.pt.lab4.service;

import com.pg.eti.pt.lab4.Lab4Application;
import com.pg.eti.pt.lab4.model.Computer;
import com.pg.eti.pt.lab4.repository.ComputerRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
//@SpringBootTest(classes = {Lab4Application.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ComputerServiceTest {

    @TestConfiguration
    static class ComputerServiceConfiguration {

        @Bean
        public ComputerService computerService() {
            return new ComputerService();
        }
    }

    @MockBean
    private ComputerRepository computerRepository;

    @Autowired
    private ComputerService computerService;

    @Before
    public void setup() {
        when(computerRepository.findById(any())).thenReturn(Optional.empty());
    }

    @Test
    public void t01_forComputerWithPrizeZero_cantAddComputerToShop() {
        Computer computer = new Computer(
                null, "Type", "Type", 4096, "Graphic", 512, 5, 0.0);
        Optional<Computer> optionalComputer = computerService.addComputer(computer);
        Assert.assertFalse(optionalComputer.isPresent());
    }

    @Test
    public void t02_forComputerWithPositivePrize_addComputerToShop() {
        Computer computer = new Computer(
                null, "Type", "Type", 4096, "Graphic", 512, 5, 12.0
        );
        Optional<Computer> optionalComputer = computerService.addComputer(computer);
        Assert.assertTrue(optionalComputer.isPresent());
    }

    @Test
    public void t03_forComputerNotExistingInShop_cantUpdateData() {
        Computer computer = new Computer(
                null, "Type", "Type", 4096, "Graphic", 512, 5, 1.0);
        Long id = 1L;
        Optional<Computer> optionalComputer = computerService.update(computer, id);
        Assert.assertFalse(optionalComputer.isPresent());
    }

    @Test
    public void t04_forComputerExisitngInShop_updateData() {
        Computer computer = new Computer(
                null, "Type", "Type", 4096, "Graphic", 512, 5, 1.0);
        when(computerRepository.findById(any())).thenReturn(Optional.of(computer));
        Long id = 1L;
        Optional<Computer> optionalComputer = computerService.update(computer, id);
        Assert.assertTrue(optionalComputer.isPresent());
    }

    @Test
    public void t05_forComputerWithAmountZero_cantAddComputerToShop(){
        Computer computer = new Computer( null, "Type", "Type", 4096, "Graphic", 512, 0, 12.0);
        Optional<Computer> optionalComputer = computerService.addComputer(computer);
        Assert.assertFalse(optionalComputer.isPresent());
    }
}
