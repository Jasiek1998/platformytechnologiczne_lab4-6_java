package com.pg.eti.pt.lab4.service;


import com.pg.eti.pt.lab4.Lab4Application;
import com.pg.eti.pt.lab4.model.Computer;
import com.pg.eti.pt.lab4.model.Order;
import com.pg.eti.pt.lab4.repository.OrderRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
//@SpringBootTest(classes = {Lab4Application.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OrderServiceTest {

    @TestConfiguration
    static class OrderServiceConfiguration {

        @Bean
        public OrderService orderService() {
            return new OrderService();
        }
    }

    @MockBean
    private OrderRepository orderRepository;

    @Autowired
    private OrderService orderService;

    @MockBean
    private ComputerService computerService;

    @Before
    public void setup() {
    }

    @Test
    public void t01_forOrderWithNoBooks_cantAddOrder() {
        Order order = new Order(null, Arrays.asList());
        Optional<Order> optionalOrder = orderService.addOrder(order);
        Assert.assertFalse(optionalOrder.isPresent());
    }

    @Test
    public void t02_forOrderWithUnavailableComputer_cantAddOrder() {
        Computer computer = new Computer(null, "Type", "Type", 4096, "Graphic", 1024, 0, 12.0);
        Order order = new Order(null, Arrays.asList(computer));
        Optional<Order> optionalOrder = orderService.addOrder(order);
        Assert.assertFalse(optionalOrder.isPresent());
    }

    @Test
    public void t03_forOrderWithAtLeastTwoAvailableComputers_addOrder() {
        Computer computer = new Computer(1L, "Type", "Type", 4096, "Graphic", 1024, 13, 12.0);
        Computer computer1 = new Computer(2L, "Type", "Type", 4096, "Graphic", 1024, 12, 12.0);
        List<Computer> computerList = new ArrayList<>();
        computerList.add(computer);
        computerList.add(computer);
        Order order = new Order();
        when(computerService.getComputerById(1L)).thenReturn(computer);
        when(computerService.getComputerById(2L)).thenReturn(computer1);
        order.setComputerList(computerList);
        Optional<Order> optionalOrder = orderService.addOrder(order);
        Assert.assertTrue(optionalOrder.isPresent());
    }
}
